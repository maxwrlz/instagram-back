const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var commentSchema = new Schema({
  content: {type: String},
  user: {type: Schema.Types.ObjectId, ref: 'User', unique: true},
  likes: [{type: Schema.Types.ObjectId, ref: 'User', default: []}],
  created_at: {type: Date, default: Date.now()}
})

commentSchema.set("toJSON", {
  transform: (document, returnObject) => {
    returnObject.id = returnObject._id.toString();
    delete returnObject._id;
    delete returnObject._v;
  },
});

module.exports = mongoose.model('Comment', commentSchema);