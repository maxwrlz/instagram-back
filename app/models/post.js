const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var postSchema = new Schema({
  user: {type: Schema.Types.ObjectId, ref: 'User', unique: true},
  images: [{type: String}],
  description: {type: String, default: ""},
  saves: [{type: Schema.Types.ObjectId, ref: 'User', default: []}],
  likes: [{type: Schema.Types.ObjectId, ref: 'User', default: []}],
  comment: [{type: Schema.Types.ObjectId, ref: 'Comment', default: []}],
  created_at: {type: Date, default: Date.now()}
})

postSchema.set("toJSON", {
  transform: (document, returnObject) => {
    returnObject.id = returnObject._id.toString();
    delete returnObject._id;
    delete returnObject._v;
  },
});

module.exports = mongoose.model('Post', postSchema);