const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var storySchema = new Schema({
  media: {type: String},
  user: {type: Schema.Types.ObjectId, ref: 'User', unique: true},
  likes: [{type: Schema.Types.ObjectId, ref: 'User', default: []}],
  views: [{type: Schema.Types.ObjectId, ref: 'Comment', default: []}],
  created_at: {type: Date, default: Date.now()}
})

storySchema.set("toJSON", {
  transform: (document, returnObject) => {
    returnObject.id = returnObject._id.toString();
    delete returnObject._id;
    delete returnObject._v;
  },
});

module.exports = mongoose.model('Story', storySchema);