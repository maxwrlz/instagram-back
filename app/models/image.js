const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var imageSchema = new Schema({
  name: {type: String, required: true},
  type: {type: String, required: true},
})

imageSchema.set("toJSON", {
  transform: (document, returnObject) => {
    returnObject.id = returnObject._id.toString();
    delete returnObject._id;
    delete returnObject._v;
  },
});

module.exports = mongoose.model('Image', imageSchema);