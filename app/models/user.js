const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var userSchema = new Schema({
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  name: {type: String, required: true},
  username: {type: String, required: true, unique: true},
  website: {type: String, default: ''},
  description: {type: String, default: ''},
  profile_picture: {type: String, default: 'default.webp'},
  account_type: {type: String, default: ''},
  nbposts: {type: Number, default: 1},
  nbsaved: {type: Number, default: 0},
  nbfollowers: {type: Number, default: 0},
  nbfollowing: {type: Number, default: 0},
  posts: [{type: Schema.Types.ObjectId, ref: 'Post', default: []}],
  saved_posts: [{type: Schema.Types.ObjectId, ref: 'Post', default: []}],
  followers: [{type: Schema.Types.ObjectId, ref: 'User', default: []}],
  following: [{type: Schema.Types.ObjectId, ref: 'User', default: []}],
  created_at: {type: Date, default: Date.now()}
})

userSchema.set("toJSON", {
  transform: (document, returnObject) => {
    returnObject.id = returnObject._id.toString();
    delete returnObject._id;
    delete returnObject._v;
  },
});

module.exports = mongoose.model('User', userSchema);