const express = require('express');
const AuthRouter = express.Router();
const bcrypt = require('../index').bcrypt;
const jwt = require('jsonwebtoken');

const User = require('../models/user');

/**
 * @swagger

 * /auth:
 *   post:
 *     tags: [User]
 *     summary: Inscription d'un utilisateur
 *     responses : 
 *        201:
 *         description: insertion d'un nouvel utilisateur dans la bdd
 *        202:
 *         description: utilisateur déjà existant
 *        400:
 *         description: champs manquants ou erreur serveur
 */
AuthRouter.post('/register', async (req, res) => {
  try {
    const checkMail = await User.findOne({email: req.body.email});
    if (checkMail) {
      res.status(202).send('Cet email est déjà relié à un autre compte');
      return;
    }
    const checkUsername = await User.findOne({username: req.body.username});
    if (checkUsername) {
      res.status(202).send('Ce nom d\'utilisateur est déjà utilisé');
      return;
    }
    const newUser = new User(req.body);
    const salt = await bcrypt.genSalt(10);
    newUser.password = await bcrypt.hash(newUser.password, salt);
    const token = jwt.sign({id: newUser.id, email: newUser.email}, process.env.JWT_KEY);
    await newUser.save().then((user) => res.status(201).send({user, token}));
  } catch (error) {
    console.log(error);
    res.status(400).send("Something went wrong... " + error);
  }
});


/**
 * @swagger

 * /auth/login:
 *   post:
 *     tags: [User]
 *     summary: Connexion d'un utilisateur
 *     responses : 
 *        200:
 *         description: récupération de l'utilisateur dans la bdd
 *        202:
 *         description: mot de passe incorrect ou utilisateur non trouvé
 *        400:
 *         description: champs manquants ou erreur serveur
 */
AuthRouter.post('/login', async (req, res) => {
  try {
    const user = await User.findOne({email: req.body.email});
    if (!user) {
      res.status(202).send('Cet email n\'est relié à aucun compte');
      return;
    }
    if (await bcrypt.compare(req.body.password, user.password)) {
      delete user.password;
      const token = jwt.sign({id: user.id, email: user.email}, process.env.JWT_KEY);
      res.status(200).send({user, token});
      return;
    } else {
      res.status(202).send("Mot de passe incorrect");
    }
  } catch (error) {
    console.log(error);
    res.status(400).send("Something went wrong... " + error);
  }
});

module.exports = AuthRouter;