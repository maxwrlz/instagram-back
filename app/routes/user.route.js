const express = require('express');
const UserRouter = express.Router();
const bcrypt = require('../index').bcrypt;

const User = require('../models/user');


/**
 * @swagger

 * /users:
 *   get:
 *     tags: [User]
 *     summary: Récupération de tous les utilisateurs
 *     responses : 
 *        200:
 *         description: retourne tous les utilisateurs de la bdd
 *        400:
 *         description: utilisateurs non trouvés ou erreur serveur
 */
UserRouter.get('/', async (_req, res) => {
  try {
    await User.find({}).then((doc) => res.status(200).send(doc));
  } catch (error) {
    console.log(error);
    res.status(400).send('Something went wrong...' + error);
  }
});


/**
 * @swagger

 * /users/edit:
 *   put:
 *     tags: [User]
 *     summary: Modification d'un user
 *     responses : 
 *        200:
 *         description: l'utilisateur a bien été modifié
 *        400:
 *         description: utilisateur non trouvé ou erreur serveur
 */
UserRouter.put('/edit', async (req, res) => {
  try {
    const body = req.body;
    const user = await User.findOne({email: body.email});
    if (!user) {
      res.status(202).send('Cet utilisateur n\'existe pas');
      return;
    }
    user.name = body.name;
    user.username = body.username;
    user.website = body.website;
    user.description = body.description;
    user.account_type = body.account_type;
    await user.save();
    res.status(201).send('Utilisateur modifié !');
  } catch (error) {
    console.log(error);
    res.status(400).send('Something went wrong...' + error);
  }
});


/**
 * @swagger

 * /users/password:
 *   put:
 *     tags: [User]
 *     summary: Modification du mot de passe d'un user
 *     responses : 
 *        200:
 *         description: le mot de passe a bien été modifié
 *        400:
 *         description: utilisateur non trouvé ou erreur serveur
 */
UserRouter.put('/password', async (req, res) => {
  try {
    const { body, token} = req;
    const user = await User.findOne({email: token.email});
    if (!user) {
      res.status(202).send('Cet utilisateur n\'existe pas');
      return;
    }
    if (await bcrypt.compare(body.password, user.password)) {
      if (await bcrypt.compare(body.newPassword, user.password)) {
        res.status(202).send('Le nouveau mot de passe doit être différent de l\'actuel');
        return;
      }
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(body.newPassword, salt);
      await user.save();
      res.status(201).send('Mot de passe modifié !');
      return;
    } else {
      res.status(202).send("Mot de passe incorrect");
    }
  } catch (error) {
    console.log(error);
    res.status(400).send('Something went wrong...' + error);
  }
});


module.exports = UserRouter;