const express = require('express');
const ImageRouter = express.Router();
const path = require('path');
const fsPromises = require('fs/promises');
const fs = require('fs');

const Image = require('../models/image');


/**
 * @swagger

 * /images:
 *   post:
 *     tags: [Images]
 *     summary: stockage d'une image sur le serveur et en bdd
 *     responses : 
 *        200:
 *         description: image stockée avec succès
 *        400:
 *         description: utilisateur non trouvé ou erreur serveur
 */
ImageRouter.post('/', async (req, res) => {
  try {
    const { image } = req.files;
    const existingImg = await Image.findOne({name: image.name.replace(' ', '')});
    if (!existingImg) {
      const newImage = new Image({
        name: String(image.name).replace(' ', ''),
        type: image.mimetype
      });
      await newImage.save();
    } else if (existingImg.type != image.mimetype) {
      const img = path.join(__dirname, '../../images/profile/'+ existingImg.name + (existingImg.type).replace('image/', '.'));
      await fsPromises.unlink(img);
      existingImg.type = image.mimetype;
      await existingImg.save();
    }
    image.mv('images/profile/' + image.name.replace(' ', '') + image.mimetype.replace('image/', '.'));
    res.status(201).send('Image successfully uploaded');
  } catch (error) {
    console.log(error);
    res.status(400).send('Something went wrong...' + error);
  }
});


/**
 * @swagger

 * /images/:type/:name:
 *   get:
 *     tags: [Images]
 *     summary: Récupération d'une image
 *     responses : 
 *        200:
 *         description: image renvoyée avec succès
 *        400:
 *         description: image ou utilisateur non trouvés ou erreur serveur
 */
ImageRouter.get('/:type/:name', async (req, res) => {
  try {
    var filePath = path.join(__dirname, '../../images/' + req.params.type + '/' + req.params.name);
    res.sendFile(filePath);
  } catch (error) {
    console.log(error);
    res.status(400).send('Something went wrong...' + error);
  }
});

module.exports = ImageRouter;