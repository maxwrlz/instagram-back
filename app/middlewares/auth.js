const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    req.token = jwt.verify(token, process.env.JWT_KEY);
    next();
  } catch (error) {
    console.log(error);
    res.status(202).send( 'Token invalide !');
  }
}