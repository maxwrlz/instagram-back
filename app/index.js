const express = require('express');
const cors = require ('cors');
const dotenv = require ('dotenv');
const mongoose = require ('mongoose');
const fileUpload = require('express-fileupload');
const app = express();
app.use(express.json());
app.use(fileUpload());
app.use(cors());
dotenv.config();

const bcrypt = require('bcryptjs');
module.exports.bcrypt = bcrypt;

const port = process.env.PORT || 3000;
const url = process.env.DATABASE_URL;

var server = require('http').Server(app);
server.listen(port, () => {
  console.log('\x1b[36m%s\x1b[0m', 'Insta API écoute désormais sur le port', port);
});


(async () => {
  try {
    mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log('\x1b[35m%s\x1b[0m', 'MongoDB Connecté !');
  } catch (err) {
    console.error(err);
  }
})();

const auth = require('./middlewares/auth');
const AuthRouter = require('./routes/auth.route');
const UserRouter = require('./routes/user.route');
const ImageRouter = require('./routes/image.route');
app.use('/auth', AuthRouter);
app.use('/users', auth, UserRouter);
app.use('/images', ImageRouter);


const swaggerUI = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Instagram API Documentation",
      version: "0.1",
      description: "Bienvenue sur la documentation de l'API Instagram",
      contact: {
        name: "API Support",
        email: "insta.contact@gmail.com",
      },
    },
  },
  apis: ["./app/routes/*.js"],
};

const specs = swaggerJsdoc(options);
app.use("/", swaggerUI.serve, swaggerUI.setup(specs));